select habitacion.numero_habitacion as NumeroDeHabitacion,
sum(precio_mantenimiento) as gasto_mantenimiento
from mantenimiento
inner join habitacion on habitacion.id_habitacion = mantenimiento.id_habitacion
where to_char(fecha_mantenimiento, 'YYYY')='2020'  
group by NumeroDeHabitacion;