/*==============================================================*/
/* DBMS name:      PostgreSQL 8                                 */
/* Created on:     02/12/2021 3:36:10                           */
/*==============================================================*/


drop table CLIENTE;

drop table COMISION;

drop table EMPLEADO;

drop table ESTADIA;

drop table FACTURA;

drop table HABITACION;

drop table MANTENIMIENTO;

drop table ROL_DE_EMPLEADO;

drop table TAXISTA;

drop table TIPO_CLIENTE;

drop table TIPO_DE_MANTENIMIENTO;

/*==============================================================*/
/* Table: CLIENTE                                               */
/*==============================================================*/
create table CLIENTE (
   ID_CLIENTE           SERIAL               not null,
   ID_TIPO_CLIENTE      INT4                 null,
   NOMBRE_CLIENTE       VARCHAR(20)          null,
   APELLIDO_CLIENTE     VARCHAR(20)          null,
   FECHA_DE_NACIMIENTO_CLIENTE DATE                 null,
   DIRECCION_CLIENTE    VARCHAR(20)          null,
   CELULAR_CLIENTE      VARCHAR(20)          null,
   constraint PK_CLIENTE primary key (ID_CLIENTE)
);

/*==============================================================*/
/* Table: COMISION                                              */
/*==============================================================*/
create table COMISION (
   ID_COMISION          SERIAL               not null,
   ID_TAXISTA           INT4                 null,
   ID_EMPLEADO          INT4                 null,
   FECHA_COMISION       DATE                 null,
   VALOR_COMISION       MONEY                null,
   constraint PK_COMISION primary key (ID_COMISION)
);

/*==============================================================*/
/* Table: EMPLEADO                                              */
/*==============================================================*/
create table EMPLEADO (
   ID_EMPLEADO          SERIAL               not null,
   ID_ROL_EMPLEADO      INT4                 null,
   NOMBRE_EMPLEADO      VARCHAR(20)          null,
   APELLIDO_EMPLEADO    VARCHAR(22)          null,
   FECHA_DE_NACIMIENTO_EMPLEADO DATE                 null,
   DIRECCION_EMPLEADO   VARCHAR(22)          null,
   GENERO_EMPLEADO      VARCHAR(22)          null,
   CELULAR_EMPLEADO     VARCHAR(10)          null,
   CELULAR2_EMPLEADO    VARCHAR(10)          null,
   SALARIO_EMPLEADO     MONEY                null,
   constraint PK_EMPLEADO primary key (ID_EMPLEADO)
);

/*==============================================================*/
/* Table: ESTADIA                                               */
/*==============================================================*/
create table ESTADIA (
   ID_ESTADIA           SERIAL               not null,
   ID_CLIENTE           INT4                 null,
   ID_EMPLEADO          INT4                 null,
   FECHA_ESTADIA        DATE                 null,
   PRECIO_ESTADIA       MONEY                null,
   constraint PK_ESTADIA primary key (ID_ESTADIA)
);

/*==============================================================*/
/* Table: FACTURA                                               */
/*==============================================================*/
create table FACTURA (
   ID_FACTURA           SERIAL               not null,
   ID_ESTADIA           INT4                 null,
   FECHA_FACTURA        DATE                 null,
   DETALLE_FACTURA      VARCHAR(50)          null,
   TOTAL_FACTURA        MONEY                null,
   constraint PK_FACTURA primary key (ID_FACTURA)
);

/*==============================================================*/
/* Table: HABITACION                                            */
/*==============================================================*/
create table HABITACION (
   ID_HABITACION        SERIAL               not null,
   ID_ESTADIA           INT4                 null,
   NUMERO_HABITACION    NUMERIC              null,
   DISPONIBILIDAD_HABITACION BOOL                 null,
   CAPACIDAD_HABITACION NUMERIC              null,
   constraint PK_HABITACION primary key (ID_HABITACION)
);

/*==============================================================*/
/* Table: MANTENIMIENTO                                         */
/*==============================================================*/
create table MANTENIMIENTO (
   ID_MANTENIMIENTO     SERIAL               not null,
   ID_TIPO_DE_MANTENIMIENTO INT4                 null,
   ID_HABITACION        INT4                 null,
   DETALLE_MANTENIMIENTO VARCHAR(22)          null,
   PRECIO_MANTENIMIENTO MONEY                null,
   FECHA_MANTENIMIENTO  DATE                 null,
   constraint PK_MANTENIMIENTO primary key (ID_MANTENIMIENTO)
);

/*==============================================================*/
/* Table: ROL_DE_EMPLEADO                                       */
/*==============================================================*/
create table ROL_DE_EMPLEADO (
   ID_ROL_EMPLEADO      SERIAL               not null,
   NOMBRE_ROL_EMPLEADO  VARCHAR(20)          null,
   constraint PK_ROL_DE_EMPLEADO primary key (ID_ROL_EMPLEADO)
);

/*==============================================================*/
/* Table: TAXISTA                                               */
/*==============================================================*/
create table TAXISTA (
   ID_TAXISTA           SERIAL               not null,
   NOMBRE_TAXISTA       VARCHAR(20)          null,
   APELLIDO_TAXISTA     VARCHAR(20)          null,
   constraint PK_TAXISTA primary key (ID_TAXISTA)
);

/*==============================================================*/
/* Table: TIPO_CLIENTE                                          */
/*==============================================================*/
create table TIPO_CLIENTE (
   ID_TIPO_CLIENTE      SERIAL               not null,
   NOMBRE_TIPO_DE_CLIENTE VARCHAR(59)          null,
   constraint PK_TIPO_CLIENTE primary key (ID_TIPO_CLIENTE)
);

/*==============================================================*/
/* Table: TIPO_DE_MANTENIMIENTO                                 */
/*==============================================================*/
create table TIPO_DE_MANTENIMIENTO (
   ID_TIPO_DE_MANTENIMIENTO SERIAL               not null,
   NOMBRE_TIPO_MANTEMINIENTO VARCHAR(30)          null,
   constraint PK_TIPO_DE_MANTENIMIENTO primary key (ID_TIPO_DE_MANTENIMIENTO)
);

alter table CLIENTE
   add constraint FK_CLIENTE_RELATIONS_TIPO_CLI foreign key (ID_TIPO_CLIENTE)
      references TIPO_CLIENTE (ID_TIPO_CLIENTE)
      on delete restrict on update restrict;

alter table COMISION
   add constraint FK_COMISION_RELATIONS_TAXISTA foreign key (ID_TAXISTA)
      references TAXISTA (ID_TAXISTA)
      on delete restrict on update restrict;

alter table COMISION
   add constraint FK_COMISION_RELATIONS_EMPLEADO foreign key (ID_EMPLEADO)
      references EMPLEADO (ID_EMPLEADO)
      on delete restrict on update restrict;

alter table EMPLEADO
   add constraint FK_EMPLEADO_RELATIONS_ROL_DE_E foreign key (ID_ROL_EMPLEADO)
      references ROL_DE_EMPLEADO (ID_ROL_EMPLEADO)
      on delete restrict on update restrict;

alter table ESTADIA
   add constraint FK_ESTADIA_RELATIONS_EMPLEADO foreign key (ID_EMPLEADO)
      references EMPLEADO (ID_EMPLEADO)
      on delete restrict on update restrict;

alter table ESTADIA
   add constraint FK_ESTADIA_RELATIONS_CLIENTE foreign key (ID_CLIENTE)
      references CLIENTE (ID_CLIENTE)
      on delete restrict on update restrict;

alter table FACTURA
   add constraint FK_FACTURA_RELATIONS_ESTADIA foreign key (ID_ESTADIA)
      references ESTADIA (ID_ESTADIA)
      on delete restrict on update restrict;

alter table HABITACION
   add constraint FK_HABITACI_RELATIONS_ESTADIA foreign key (ID_ESTADIA)
      references ESTADIA (ID_ESTADIA)
      on delete restrict on update restrict;

alter table MANTENIMIENTO
   add constraint FK_MANTENIM_RELATIONS_TIPO_DE_ foreign key (ID_TIPO_DE_MANTENIMIENTO)
      references TIPO_DE_MANTENIMIENTO (ID_TIPO_DE_MANTENIMIENTO)
      on delete restrict on update restrict;

alter table MANTENIMIENTO
   add constraint FK_MANTENIM_RELATIONS_HABITACI foreign key (ID_HABITACION)
      references HABITACION (ID_HABITACION)
      on delete restrict on update restrict;

