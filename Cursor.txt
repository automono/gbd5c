do $$
declare 
	tabla record;
	habitaciones cursor for select habitacion.numero_habitacion as NumeroHabitacion,
	estadia.fecha_estadia as FechaEstadia, estadia.precio_estadia as ValorEstadia
	from habitacion
	inner join estadia on estadia.id_habitacion=habitacion.id_habitacion
	where to_char(fecha_estadia, 'YYYY')= '2020';
begin
open habitaciones;
fetch habitaciones into tabla;
while (found) loop
raise notice 'Habitaciones: %, Fecha de estadia: %, Precio de Estadia: %', tabla.NumeroHabitacion,
tabla.FechaEstadia, tabla.ValorEstadia;
fetch habitaciones into tabla;
end loop;
end $$